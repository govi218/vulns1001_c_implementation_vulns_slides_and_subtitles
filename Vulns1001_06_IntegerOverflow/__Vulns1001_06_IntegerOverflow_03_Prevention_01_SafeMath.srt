1
00:00:00,160 --> 00:00:04,319
when it comes to preventing integer

2
00:00:01,920 --> 00:00:06,960
overflows I want to first talk about the

3
00:00:04,319 --> 00:00:09,040
very common anti-patterns so for

4
00:00:06,960 --> 00:00:10,320
instance the intuition would suggest

5
00:00:09,040 --> 00:00:12,960
that the first thing you should do is

6
00:00:10,320 --> 00:00:14,320
just do something like if a plus b is

7
00:00:12,960 --> 00:00:16,400
greater than c

8
00:00:14,320 --> 00:00:19,279
then you know we know that this thing is

9
00:00:16,400 --> 00:00:20,960
going to be incorrect but if a or b are

10
00:00:19,279 --> 00:00:23,439
attacker controlled data then

11
00:00:20,960 --> 00:00:26,240
unfortunately when you do the a plus b

12
00:00:23,439 --> 00:00:28,320
you could have already overflowed before

13
00:00:26,240 --> 00:00:30,640
the sanity check occurs and consequently

14
00:00:28,320 --> 00:00:32,719
this will be a low number instead of the

15
00:00:30,640 --> 00:00:35,760
number that's greater than C as you were

16
00:00:32,719 --> 00:00:37,440
expecting also it can introduce issues

17
00:00:35,760 --> 00:00:39,200
with signedness

18
00:00:37,440 --> 00:00:41,520
that we'll talk about in the other

19
00:00:39,200 --> 00:00:43,520
integer issues section in the future of

20
00:00:41,520 --> 00:00:45,200
course if either of these are signed

21
00:00:43,520 --> 00:00:47,600
then you might you know wrap around from

22
00:00:45,200 --> 00:00:49,200
being a big number to a small number and

23
00:00:47,600 --> 00:00:50,800
again that small number is not going to

24
00:00:49,200 --> 00:00:52,160
be greater than this big number as you

25
00:00:50,800 --> 00:00:54,320
were expecting

26
00:00:52,160 --> 00:00:56,000
now sometimes it's recommended to

27
00:00:54,320 --> 00:00:57,920
basically do the mathematically

28
00:00:56,000 --> 00:01:00,160
equivalent thing and subtract b from

29
00:00:57,920 --> 00:01:02,000
both sides and so if you have this big

30
00:01:00,160 --> 00:01:04,239
number minus b and you know the

31
00:01:02,000 --> 00:01:06,240
presumption is that b is going to be

32
00:01:04,239 --> 00:01:08,560
less than C and so consequently you know

33
00:01:06,240 --> 00:01:10,080
that can't underflow and then you know

34
00:01:08,560 --> 00:01:12,320
you will find mathematically

35
00:01:10,080 --> 00:01:14,320
equivalently whether or not a plus b

36
00:01:12,320 --> 00:01:16,400
would have overflowed

37
00:01:14,320 --> 00:01:18,159
and caused you know a incorrect sanity

38
00:01:16,400 --> 00:01:20,240
check now the problem of course with

39
00:01:18,159 --> 00:01:23,280
this is that this in and of itself is

40
00:01:20,240 --> 00:01:25,040
insufficient because uh if b is attacker

41
00:01:23,280 --> 00:01:27,040
controlled then they could just set it

42
00:01:25,040 --> 00:01:29,840
to a big number which consequently will

43
00:01:27,040 --> 00:01:32,159
lead to a small minus big integer

44
00:01:29,840 --> 00:01:34,079
underflow and the sanity check will be

45
00:01:32,159 --> 00:01:35,520
bypassed there as well so something like

46
00:01:34,079 --> 00:01:37,200
this only works if you're adding in a

47
00:01:35,520 --> 00:01:39,280
whole bunch of extra sanity checks of

48
00:01:37,200 --> 00:01:40,880
you know is a less than C and b less

49
00:01:39,280 --> 00:01:43,840
than c

50
00:01:40,880 --> 00:01:45,200
and then also sometimes people do a is a

51
00:01:43,840 --> 00:01:47,680
plus b

52
00:01:45,200 --> 00:01:49,520
greater than or sorry less than a and

53
00:01:47,680 --> 00:01:51,920
the idea is that if this integer

54
00:01:49,520 --> 00:01:53,920
overflows and wraps around then all of a

55
00:01:51,920 --> 00:01:56,159
sudden these values will become less

56
00:01:53,920 --> 00:01:58,079
than you know this initial value and you

57
00:01:56,159 --> 00:01:59,759
know that gets closer to being something

58
00:01:58,079 --> 00:02:00,960
that might be okay

59
00:01:59,759 --> 00:02:02,320
but then you have to start worrying

60
00:02:00,960 --> 00:02:05,040
about whether or not you're accounting

61
00:02:02,320 --> 00:02:07,200
for sinus correctly right so a plus b

62
00:02:05,040 --> 00:02:08,319
less than a might be what you expect

63
00:02:07,200 --> 00:02:10,479
when you're dealing with unsigned

64
00:02:08,319 --> 00:02:12,959
numbers but you know do you want to

65
00:02:10,479 --> 00:02:16,239
allow something like a minus 1 and b

66
00:02:12,959 --> 00:02:18,319
minus 2 because a plus b is you know

67
00:02:16,239 --> 00:02:20,640
legitimately less than a when those are

68
00:02:18,319 --> 00:02:22,319
assigned to numbers and that's not the

69
00:02:20,640 --> 00:02:23,520
sort of integer overflow that you were

70
00:02:22,319 --> 00:02:25,280
looking for

71
00:02:23,520 --> 00:02:27,440
so you know it's actually extremely

72
00:02:25,280 --> 00:02:29,520
difficult to do these sanity checks

73
00:02:27,440 --> 00:02:31,280
right and to detect integer overflows

74
00:02:29,520 --> 00:02:34,239
for all of these signed and unsigned

75
00:02:31,280 --> 00:02:37,920
cases so let me just instead introduce

76
00:02:34,239 --> 00:02:38,959
you to safe math safe math is a pleasant

77
00:02:37,920 --> 00:02:41,280
path

78
00:02:38,959 --> 00:02:43,760
so how does one do safe math well the

79
00:02:41,280 --> 00:02:46,239
answer is built-in compiler intrinsics

80
00:02:43,760 --> 00:02:48,400
mostly and this might be the coolest

81
00:02:46,239 --> 00:02:50,319
thing you've never heard of and I assume

82
00:02:48,400 --> 00:02:52,400
you've never heard of it because I see

83
00:02:50,319 --> 00:02:53,120
it so infrequently

84
00:02:52,400 --> 00:02:55,760
so

85
00:02:53,120 --> 00:02:57,760
clang has these mechanisms these

86
00:02:55,760 --> 00:03:00,000
built-in compiler intrinsics called

87
00:02:57,760 --> 00:03:02,239
built-in ad overflow sub overflow and

88
00:03:00,000 --> 00:03:03,680
multiply overflow and there are versions

89
00:03:02,239 --> 00:03:05,519
that just you know

90
00:03:03,680 --> 00:03:07,280
don't care about the type and you just

91
00:03:05,519 --> 00:03:09,680
use that or there's versions where if

92
00:03:07,280 --> 00:03:11,920
you want to explicitly specify the type

93
00:03:09,680 --> 00:03:14,959
you can do that as well clang has that

94
00:03:11,920 --> 00:03:17,519
and gcc has the exact same things using

95
00:03:14,959 --> 00:03:19,280
the exact same semantics so that's good

96
00:03:17,519 --> 00:03:21,599
that allows for

97
00:03:19,280 --> 00:03:23,840
compiler compatibility

98
00:03:21,599 --> 00:03:26,959
and then Windows has something similar

99
00:03:23,840 --> 00:03:29,599
in kernel space it has nt int safe which

100
00:03:26,959 --> 00:03:31,840
adds a bunch of things like rtl long add

101
00:03:29,599 --> 00:03:33,760
so in compiler sorry in kernel space you

102
00:03:31,840 --> 00:03:35,840
can avoid things like integer overflows

103
00:03:33,760 --> 00:03:38,640
on your ad operations etc

104
00:03:35,840 --> 00:03:40,480
I don't know yet whether they have a

105
00:03:38,640 --> 00:03:41,920
user space version hopefully I will edit

106
00:03:40,480 --> 00:03:43,440
this video in the future if I find the

107
00:03:41,920 --> 00:03:45,120
user space version but for right now i

108
00:03:43,440 --> 00:03:47,440
haven't found it

109
00:03:45,120 --> 00:03:49,920
so let's talk about how you use safe

110
00:03:47,440 --> 00:03:52,400
math we are going from an ACID bath to a

111
00:03:49,920 --> 00:03:55,760
pleasant path all right so here was you

112
00:03:52,400 --> 00:03:58,400
know here is a very trivial uh integer

113
00:03:55,760 --> 00:04:00,319
overflow so we've got unsigned into a

114
00:03:58,400 --> 00:04:02,959
we're using a to I to bring in attack

115
00:04:00,319 --> 00:04:05,760
controlled value on sign in b on sign in

116
00:04:02,959 --> 00:04:08,159
c so a plus b equals C and we would

117
00:04:05,760 --> 00:04:11,120
expect this to wrap around if an

118
00:04:08,159 --> 00:04:13,599
attacker provides arc v values that are

119
00:04:11,120 --> 00:04:17,919
close to the maximum values

120
00:04:13,599 --> 00:04:20,639
so to do this with safe math instead

121
00:04:17,919 --> 00:04:23,199
we can have this explicit version

122
00:04:20,639 --> 00:04:24,560
which uses a and I say explicit here

123
00:04:23,199 --> 00:04:25,680
because I'll show you a cursor version

124
00:04:24,560 --> 00:04:27,840
later

125
00:04:25,680 --> 00:04:29,680
uses an explicit variable bool

126
00:04:27,840 --> 00:04:32,160
overflowed and you set it to false to

127
00:04:29,680 --> 00:04:34,720
start with then you use the built-in add

128
00:04:32,160 --> 00:04:38,400
overflow and what does this do it does a

129
00:04:34,720 --> 00:04:41,919
plus b and it stores the result into c

130
00:04:38,400 --> 00:04:44,240
then it returns from this intrinsic

131
00:04:41,919 --> 00:04:46,240
whether or not the result of a and b

132
00:04:44,240 --> 00:04:47,840
overflowed so it's still going to put

133
00:04:46,240 --> 00:04:49,759
the result in C whether or not it

134
00:04:47,840 --> 00:04:51,360
overflowed and then it's up to you to

135
00:04:49,759 --> 00:04:53,199
decide whether you care that it

136
00:04:51,360 --> 00:04:56,240
overflowed or not which for purposes of

137
00:04:53,199 --> 00:04:58,080
this class you almost always care it has

138
00:04:56,240 --> 00:05:00,639
to be a sort of very rare day where

139
00:04:58,080 --> 00:05:03,039
you're intentionally overflowing

140
00:05:00,639 --> 00:05:04,560
right so basically if that overflowed

141
00:05:03,039 --> 00:05:06,560
then you print out something saying you

142
00:05:04,560 --> 00:05:08,479
know it overflowed and you error out so

143
00:05:06,560 --> 00:05:10,880
if we go ahead and run that we can see

144
00:05:08,479 --> 00:05:13,600
that one plus two is three and that

145
00:05:10,880 --> 00:05:16,000
works fine but one plus four billion

146
00:05:13,600 --> 00:05:18,000
something which corresponds to all f's

147
00:05:16,000 --> 00:05:20,560
would lead to an integer overflow which

148
00:05:18,000 --> 00:05:22,560
is correctly caught and then alerted and

149
00:05:20,560 --> 00:05:24,880
exited in this code

150
00:05:22,560 --> 00:05:27,199
so like I said you can do it explicitly

151
00:05:24,880 --> 00:05:28,720
with a overflow variable to tell you and

152
00:05:27,199 --> 00:05:30,479
allow you to check explicitly that

153
00:05:28,720 --> 00:05:32,560
there's an overflow or you can just you

154
00:05:30,479 --> 00:05:34,560
know put this into you know a

155
00:05:32,560 --> 00:05:36,960
conditional statement because again it's

156
00:05:34,560 --> 00:05:39,039
returning a boolean where it's true if

157
00:05:36,960 --> 00:05:41,280
an overflow occurred as a result of

158
00:05:39,039 --> 00:05:43,919
adding these two values together

159
00:05:41,280 --> 00:05:46,800
the thing that you need to not do

160
00:05:43,919 --> 00:05:48,639
is add in this built-in ad overflow and

161
00:05:46,800 --> 00:05:51,120
magically assume that it's going to

162
00:05:48,639 --> 00:05:53,759
error out and return for you I've seen

163
00:05:51,120 --> 00:05:55,039
this in code basically you still have to

164
00:05:53,759 --> 00:05:56,960
check whether or not an overflow

165
00:05:55,039 --> 00:05:58,800
occurred you can't just throw it in and

166
00:05:56,960 --> 00:06:01,199
expect it to magically fix your ACID

167
00:05:58,800 --> 00:06:03,680
path and additionally if we were to

168
00:06:01,199 --> 00:06:05,600
consider the case of signed integers

169
00:06:03,680 --> 00:06:08,160
instead of unsigned well the good news

170
00:06:05,600 --> 00:06:10,960
is you can use exactly the same safe

171
00:06:08,160 --> 00:06:13,600
math for a safe and pleasant bath in

172
00:06:10,960 --> 00:06:15,520
order to do the overflow check and then

173
00:06:13,600 --> 00:06:17,759
error out if appropriate

174
00:06:15,520 --> 00:06:19,680
again same thing we've got one and

175
00:06:17,759 --> 00:06:22,880
negative one that's zero that shouldn't

176
00:06:19,680 --> 00:06:25,039
be an error one and the extremely large

177
00:06:22,880 --> 00:06:27,840
value which interpreted as a signed

178
00:06:25,039 --> 00:06:28,800
value is a negative one and that's not

179
00:06:27,840 --> 00:06:31,120
an error

180
00:06:28,800 --> 00:06:33,280
one and two is not an error but one and

181
00:06:31,120 --> 00:06:35,840
two billion which corresponds to the

182
00:06:33,280 --> 00:06:38,639
largest possible positive value for a

183
00:06:35,840 --> 00:06:40,960
signed 32-bit integer that will overflow

184
00:06:38,639 --> 00:06:42,880
that will wrap around from 2 billion to

185
00:06:40,960 --> 00:06:45,360
all of a sudden being you know negative

186
00:06:42,880 --> 00:06:47,280
2 billion so that's not good and this is

187
00:06:45,360 --> 00:06:49,840
successfully detected by the compiler

188
00:06:47,280 --> 00:06:49,840
intrinsic


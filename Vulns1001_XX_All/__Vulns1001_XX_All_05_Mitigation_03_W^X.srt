1
00:00:00,399 --> 00:00:04,799
the goal of the non-executable memory

2
00:00:02,560 --> 00:00:06,799
exploit mitigation is to not let

3
00:00:04,799 --> 00:00:08,320
attackers just write code into memory

4
00:00:06,799 --> 00:00:10,240
and then execute it

5
00:00:08,320 --> 00:00:12,080
so if we think about our you know

6
00:00:10,240 --> 00:00:14,320
example buffer overflow that we've seen

7
00:00:12,080 --> 00:00:16,080
thus far memcpying asset attacker

8
00:00:14,320 --> 00:00:17,920
control data into a vulnerable buffer

9
00:00:16,080 --> 00:00:19,920
and overflowing

10
00:00:17,920 --> 00:00:22,000
well you know one of the canonical

11
00:00:19,920 --> 00:00:24,320
methods that historically when the first

12
00:00:22,000 --> 00:00:26,320
sort of buffer overflows were achieved

13
00:00:24,320 --> 00:00:28,800
what they would do is this return

14
00:00:26,320 --> 00:00:30,480
address it needs to jump somewhere into

15
00:00:28,800 --> 00:00:33,200
attack controlled code

16
00:00:30,480 --> 00:00:35,760
the shell code and so what they would do

17
00:00:33,200 --> 00:00:38,239
is they would return into code that was

18
00:00:35,760 --> 00:00:41,040
found in the buffer itself

19
00:00:38,239 --> 00:00:44,000
so what do you do with non-executable

20
00:00:41,040 --> 00:00:46,399
stack you say okay boom that memory is

21
00:00:44,000 --> 00:00:48,640
not executable so to render memory

22
00:00:46,399 --> 00:00:50,879
non-executable means that the memory

23
00:00:48,640 --> 00:00:53,199
management system must have some say in

24
00:00:50,879 --> 00:00:54,960
it and therefore this means that it has

25
00:00:53,199 --> 00:00:57,520
to be supported by the operating system

26
00:00:54,960 --> 00:00:59,600
virtualization system or firmware so

27
00:00:57,520 --> 00:01:01,199
again if you are the programmer for the

28
00:00:59,600 --> 00:01:03,120
operating system virtualization or

29
00:01:01,199 --> 00:01:05,439
firmware that means this would be your

30
00:01:03,120 --> 00:01:06,960
job in order to implement this exploit

31
00:01:05,439 --> 00:01:08,640
mitigation

32
00:01:06,960 --> 00:01:10,720
so talking a little more you know

33
00:01:08,640 --> 00:01:12,479
robustly and thoroughly about it the

34
00:01:10,720 --> 00:01:15,040
property that we say people are trying

35
00:01:12,479 --> 00:01:17,119
to achieve is what is called write x or

36
00:01:15,040 --> 00:01:19,920
execute so if you remember your truth

37
00:01:17,119 --> 00:01:23,200
tables you know if right is one and

38
00:01:19,920 --> 00:01:25,040
execute is one the result is zero false

39
00:01:23,200 --> 00:01:27,119
right so basically this could only be

40
00:01:25,040 --> 00:01:29,360
true if one of these is set and the

41
00:01:27,119 --> 00:01:31,680
other is not set so we want right with

42
00:01:29,360 --> 00:01:33,360
execute to false or execute with right

43
00:01:31,680 --> 00:01:35,040
to false so we can have executable

44
00:01:33,360 --> 00:01:36,960
memory that's not writable writable

45
00:01:35,040 --> 00:01:39,840
memory that's not executable

46
00:01:36,960 --> 00:01:41,840
so the idea is that data locations like

47
00:01:39,840 --> 00:01:44,000
the stack or the heap should just

48
00:01:41,840 --> 00:01:46,320
generally never be executable these are

49
00:01:44,000 --> 00:01:49,040
places for data the code is mapped by

50
00:01:46,320 --> 00:01:51,040
the execution environment somewhere else

51
00:01:49,040 --> 00:01:54,399
now unfortunately you know modern

52
00:01:51,040 --> 00:01:56,560
systems and optimization required for

53
00:01:54,399 --> 00:01:58,479
just in time compilation of languages

54
00:01:56,560 --> 00:02:00,240
like javascript where they have to be

55
00:01:58,479 --> 00:02:02,399
just in time compiled in order to

56
00:02:00,240 --> 00:02:03,759
achieve adequate performance well

57
00:02:02,399 --> 00:02:06,399
unfortunately these make it a little bit

58
00:02:03,759 --> 00:02:08,399
harder because essentially what it means

59
00:02:06,399 --> 00:02:10,319
is that all of a sudden on demand this

60
00:02:08,399 --> 00:02:12,879
attack controlled javascript comes in

61
00:02:10,319 --> 00:02:15,440
over the network and then it the you

62
00:02:12,879 --> 00:02:17,520
know browser the javascript engine is

63
00:02:15,440 --> 00:02:19,680
responsible for taking that interpreted

64
00:02:17,520 --> 00:02:22,400
language and compiling it down to native

65
00:02:19,680 --> 00:02:24,239
code just in time that native code has

66
00:02:22,400 --> 00:02:26,720
to go somewhere that native code has to

67
00:02:24,239 --> 00:02:28,160
be written on demand and then executed

68
00:02:26,720 --> 00:02:30,000
so unfortunately

69
00:02:28,160 --> 00:02:33,360
these tend to become you know large

70
00:02:30,000 --> 00:02:34,840
holes in the write xor execute

71
00:02:33,360 --> 00:02:37,360
exploit mitigation

72
00:02:34,840 --> 00:02:39,680
technique additionally

73
00:02:37,360 --> 00:02:41,360
if an attacker has the ability to get

74
00:02:39,680 --> 00:02:44,160
code execution instead of you know just

75
00:02:41,360 --> 00:02:46,480
jumping into their code immediately

76
00:02:44,160 --> 00:02:47,840
they can potentially call to some other

77
00:02:46,480 --> 00:02:49,440
function so instead of returning to

78
00:02:47,840 --> 00:02:51,680
their code they return to some other

79
00:02:49,440 --> 00:02:54,239
function and there are you know standard

80
00:02:51,680 --> 00:02:57,040
APIs like mprotect or virtual protect on

81
00:02:54,239 --> 00:02:59,920
Windows that are APIs specifically for

82
00:02:57,040 --> 00:03:02,400
the purpose of changing the permissions

83
00:02:59,920 --> 00:03:04,400
on memory and so you could have started

84
00:03:02,400 --> 00:03:07,920
out with something like the stack or the

85
00:03:04,400 --> 00:03:09,760
heap marked as write only or read write

86
00:03:07,920 --> 00:03:11,200
and then if the attacker has the ability

87
00:03:09,760 --> 00:03:13,040
to call one of these functions they can

88
00:03:11,200 --> 00:03:16,159
go ahead and mark it executable so let's

89
00:03:13,040 --> 00:03:18,800
rewrite execute some platforms like iOS

90
00:03:16,159 --> 00:03:20,640
actually you know explicitly disallow

91
00:03:18,800 --> 00:03:22,959
APIs like this from running in the

92
00:03:20,640 --> 00:03:25,599
context of an application running in

93
00:03:22,959 --> 00:03:27,280
user space and furthermore they will

94
00:03:25,599 --> 00:03:30,560
even go so far as to use hardware

95
00:03:27,280 --> 00:03:32,400
modifications in order to backstop the

96
00:03:30,560 --> 00:03:34,720
permissions in kernel space so they'll

97
00:03:32,400 --> 00:03:36,799
use actual registers that lock regions

98
00:03:34,720 --> 00:03:38,799
of memory to say these things shall

99
00:03:36,799 --> 00:03:41,200
never be executable

100
00:03:38,799 --> 00:03:42,000
and so no matter what games you play

101
00:03:41,200 --> 00:03:43,680
with

102
00:03:42,000 --> 00:03:46,720
trying to change permissions it'll never

103
00:03:43,680 --> 00:03:49,120
be executable it'll only always be

104
00:03:46,720 --> 00:03:50,879
readable or writeable but non-executable

105
00:03:49,120 --> 00:03:52,400
so hardware mechanisms can definitely

106
00:03:50,879 --> 00:03:54,959
help there but they're not generally

107
00:03:52,400 --> 00:03:57,519
available and in most general purpose

108
00:03:54,959 --> 00:03:59,280
operating systems it's not the case that

109
00:03:57,519 --> 00:04:01,360
these APIs are actually restricted in

110
00:03:59,280 --> 00:04:03,200
any way that would stop an attacker

111
00:04:01,360 --> 00:04:05,920
so as mentioned before the execution

112
00:04:03,200 --> 00:04:07,680
environment must support this but unlike

113
00:04:05,920 --> 00:04:09,680
some of these other exploit mitigations

114
00:04:07,680 --> 00:04:11,360
generally speaking

115
00:04:09,680 --> 00:04:13,360
you the programmer don't have to

116
00:04:11,360 --> 00:04:15,040
actually opt into it so the operating

117
00:04:13,360 --> 00:04:17,680
system maker the virtualization system

118
00:04:15,040 --> 00:04:19,440
maker the firmware maker must enable

119
00:04:17,680 --> 00:04:21,440
this non-executable memory they must you

120
00:04:19,440 --> 00:04:24,080
know design the memory management

121
00:04:21,440 --> 00:04:26,320
subsystem to enable this mechanism mark

122
00:04:24,080 --> 00:04:29,040
the stack as non-executable but then

123
00:04:26,320 --> 00:04:31,600
afterwards all the other applications or

124
00:04:29,040 --> 00:04:33,600
os's or you know the firmware itself

125
00:04:31,600 --> 00:04:35,040
doesn't necessarily have to opt in but

126
00:04:33,600 --> 00:04:36,960
if there are any you know meaningful

127
00:04:35,040 --> 00:04:40,639
caveats we will go ahead and document

128
00:04:36,960 --> 00:04:40,639
those on the website for you to see

